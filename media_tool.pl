#!/usr/bin/perl
use strict;
use Capture::Tiny ':all';
use DateTime::Format::Strptime;
use Getopt::Long;
use Pod::Usage;
use JSON;
use LWP::Simple;
use POSIX qw(strftime);
use REST::Client;
use IO::Handle;
use URI::Encode;
use Env;

STDOUT->autoflush(1);
STDERR->autoflush(1);

# If enabled, disable mail output and print it at the build log
my $debug = 0;
my $no_cleanup = 0;
my $help = 0;
my $man = 0;
my $test;
my $branch_to_test = undef;

GetOptions(
	"debug" => \$debug,
	"no-cleanup" => \$no_cleanup,
	"test=s" => \$test,
	"help|?" => \$help,
	man => \$man
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

my $start_script_time = time();

# Make sure it will use ccache to speedup builds
$ENV{'MAKEFLAGS'} = "-j9";
$ENV{'CCACHE_DIR'} = "/var/lib/jenkins/.ccache";
$ENV{'PATH'} = "/usr/lib/ccache:/var/lib/jenkins/.local/bin:$PATH";

if ($test) {
	$debug =1;
	my $uri = URI::Encode->new({encode_reserved => 1});
	$test = $uri->encode($test);
}

if ($debug > 1) {
	use Data::Dumper;
}

my $patchwork_instance = 'https://patchwork.linuxtv.org';

# Master branch
my $master_branch = "master";

# Working branch to use while handling patches
my $tmpbranch="__tmp_pr_handler";

# Email for maintainers/MLs
my $maintainers="mchehab+samsung\@kernel.org, linux-media\@vger.kernel.org";

my $check = "compile_checks";

my $home = "/var/lib/jenkins";

my $work_dir = "$home/auto_handler";

my $suppress_warnings = "$work_dir/sparse_false_warnings.txt";

#my $gcc="gcc -Wcounterexamples -fno-diagnostics-show-caret";
my $gcc="gcc -fno-diagnostics-show-caret";

my @make_yescmd = (
	"make O=build_yes CC='$gcc' CF=-D__CHECK_ENDIAN__ CONFIG_DEBUG_SECTION_MISMATCH=y C=1 W=1 CHECK=$check drivers/staging/media/",
	"make O=build_yes CC='$gcc' CF=-D__CHECK_ENDIAN__ CONFIG_DEBUG_SECTION_MISMATCH=y C=1 W=1 CHECK=$check drivers/media/",
);

# Don't need to turn on checks/warnings at the second build
my @make_modcmd = (
	"make O=build_mod CC='$gcc' drivers/staging/media/",
	"make O=build_mod CC='$gcc' CHECK=$check drivers/media/",
);

my @doc_builds = (
	"make O=build_yes SPHINXDIRS='media' htmldocs",
);

my @per_pr_builds = (
	"make O=build_yes SPHINXDIRS='media' pdfdocs",
);

my $checkpatch = "formail -c | ./scripts/checkpatch.pl --terse --mailback --no-summary --strict";

#
# Global vars
#

my $reference_cs;
my $final_patch;
my $pull_log;
my $devel_tree;
my $devel_branch;
my %lines_changed_pull_email;
my %lines_changed_git_pull;
my @ignore;
my $email_from;
my $email_subject;
my $email_msg_id;
my $mbox;
my $error;

#
# Seek for the closest branch
#
sub reset_branch() {
	my $min = 99999999;
	my $best_base;
	my @branches;

	# just move to the FETCH_HEAD and warn user
	printf "\tReseting to FETCH_HEAD and hoping for the best.\n";
	if (system("git reset --hard FETCH_HEAD")) {
		return;
	}

	printf "\tSeeking for the best merge origin\n";

	# Get staging branches
	push @branches, $reference_cs;
	open IN, "git branch|";
	while (<IN>) {
		if (m,^\s*(staging\/.*)\n,) {
			push @branches, $1;
		}
		if (m,^\s*(v4l_for_linus.*)\n,) {
			push @branches, $1;
		}
	}
	close IN;

	# Get last -rc branch and last stage one
	open IN, "git tag|";
	my ($rc, $last);
	while (<IN>) {
		if (!m,^(.*-rc\d+),) {
			$rc = $1;
		} else {
			$last = $_;
			$last =~ s/\s+$//;
		}
	}
	close IN;
	push @branches, $rc if ($rc);
	push @branches, $last if ($last);

	# Seek for the best reference branch among the above ones
	foreach my $i (@branches) {
		printf "\tBranch %s ", $i;

		my $base = qx(git merge-base $i FETCH_HEAD);
		$base =~ s/\s+$//;
		my $count = qx(git log $base.. --oneline|wc -l);
		$count =~ s/\s+$//;

		printf "\tis $count patches behind\n";
		if ($count < $min) {
			$min = $count;
			$best_base = $i;
			$reference_cs = $base;
		}
	}
	printf "\tBest reference branch: $best_base ($min patches), merge base = $reference_cs\n";

	return;
}

sub branch_to_quilt() {
	my ($remote, $head) = media_stage_head();

	my @files = qx(git format-patch -k -o patches $head..$branch_to_test);
	if ($?) {
		$error = "FAILED: can't fetch patches from $head to $branch_to_test";
		return;
	}

	if (!@files) {
		$error = "FAILED: no new patches in this tree";
		return;
	}

	# Move the existing series to an old file
	qx(mv patches/series patches/series.old);

	# write the new patches series
	open IN, "patches/series.old";
	open OUT, ">patches/series";
	printf OUT "# GIT Patches from $head $branch_to_test\n";
	foreach my $i (@files) {
		$i =~ s/\s+$//;
		$i =~ s,^patches/,,;
		printf OUT "$i\n";
	}
	printf OUT "\n";
	while (<IN>) {
		printf OUT $_;
	}
	close OUT;
	close IN;
	unlink("patches/series.old");

	# Create temporary branch or die
	if (system("git checkout -q -b $tmpbranch $remote/master")) {
		print("FAILED: probably the tree doesn't exist");
		exit(1);
	}
}

#
# Convert a branch into a quilt tree
#
sub pull_request_to_quilt() {
	if ($devel_tree =~ m,^ssh://([^\/]+),) {
		$error = "FAILED: Jenkins doesn't have a ssh account at $1";
		return;
	}

	# Create temporary branch or die
	if (system("git checkout -q -b $tmpbranch $master_branch")) {
		# On error, just use the current reference to create it
		if (system("git checkout -q -b $tmpbranch")) {
			qx(git checkout -q -f $master_branch);
			qx(git branch -D $tmpbranch);

			$error = "FAILED: probably the tree doesn't exist";

			return;
		}
	}

	#
	# Pull patches from developer's branch into the temporary branch
	#
	# If errors happen, do the hard way: use the fetch head
	# and seek for the closest branch for the reference CS
	#
	printf("\tImporting patches from $devel_tree $devel_branch\n");
	unless (open IN, "git pull -q --no-edit --ff $devel_tree $devel_branch |") {
		if (reset_branch()) {
			qx(git checkout -q -f $master_branch);
			qx(git branch -D $tmpbranch);
			$error = "FAILED: it sounds that the branch is not a clone of $master_branch branch";
			return;
		}
	} else {
		while (<IN>) {
			$pull_log .= $_;
			if (m/^\s+(\S+)\s+\|\s+(\d+)/) {
				$lines_changed_git_pull{$1} = $2;
			}
			if (m/^(fatal:.*)\n/) {
				$error = $1;
				return;
			}
		}
		close IN;
	}
	my $gpg = qx(git verify-tag FETCH_HEAD 2>&1);
	print $gpg;

	# Save the files under patches/
	my @files;
	if ($final_patch) {
		@files = qx(git format-patch -k -o patches $reference_cs..$final_patch);
		if ($?) {
			$error = "FAILED: can't fetch patches from $reference_cs to $final_patch";
			return;
		}

		# If final_patch is bogus, discards it and tries again
		$final_patch= "" if (!@files);
	}

	if (!$final_patch) {
		@files = qx(git format-patch -k -o patches $reference_cs..FETCH_HEAD);
		if ($?) {
			$error = "FAILED: can't fetch patches from $reference_cs to FETCH_HEAD";
			return;
		}
	}

	if (!@files) {
		$error = "FAILED: no new patches in this tree";
		return;
	}

	# Move the existing series to an old file
	qx(mv patches/series patches/series.old);

	# write the new patches series
	open IN, "patches/series.old";
	open OUT, ">patches/series";
	printf OUT "# GIT Patches from $devel_tree $devel_branch\n";
	foreach my $i (@files) {
		$i =~ s/\s+$//;
		$i =~ s,^patches/,,;
		printf OUT "$i\n";
	}
	printf OUT "\n";
	while (<IN>) {
		printf OUT $_;
	}
	close OUT;
	close IN;
	unlink("patches/series.old");

	# Return to the current staging branch and delete the temporary one
	qx(git checkout -q -f $master_branch);
	qx(git branch -D $tmpbranch);

	if (!$final_patch) {
		printf "\nWARNING: final patch was not set\n";
	}
	return $gpg;
}

#
# Handle a request created by "git request-pull"
#
sub handle_pull_request_email($)
{
	my $url = shift;
	my ($file, $diffstat, $content);

	$content = get($url);

	if (!$content) {
		return "FAILED: Can't connect to $url";
	};

	$content =~ s/\s+$//;

	my $tmpfile = "__tmp_PR_file";
	open OUT, "> $tmpfile";
	print OUT $content;
	close OUT;

	if (!open IN, "cat $tmpfile | formail -b  -I Content-Type -I MIME-Version -I Content-Transfer-Encoding -I Date -I X-Patchwork-Id -I To -I Cc |") {
		return "FAILED: can't recognize mbox format";
	}
	my $header = 1;
	while (<IN>) {
		if ($header) {
			if (m/^\n/) {
				$header = 0;
				next;
			}
			if (m/From:\s*(.*)/i) {
				$email_from = $1;
				printf "\tEmail from   : $email_from\n";
				next;
			}
			if (m/Subject:\s*(.*)/i) {
				$email_subject = $1;
				printf "\tEmail Subject: $email_subject\n";
				next;
			}
			if (m/Message-Id:\s*(.*)/i) {
				$email_msg_id = $1;
				printf "\tEmail Msg ID : $email_msg_id\n";
				next;
			}
			next;
		}
		if (m/^\s+(\S+)\s+\|\s+(\d+)/) {
			$lines_changed_pull_email{$1} = $2;
			$diffstat .= $_;
			next;
		}
		if (m/The following changes? since commit ([\da-f]+):/) {
			$reference_cs = $1;
			printf "\tFrom changeset: $reference_cs\n";
			next;
		}
		if (m/for you to fetch changes? up to ([\da-f]+):/) {
			$final_patch = $1;
			printf "\tTo   changeset: $final_patch\n";
			next;
		}

		$file .= $_;
	}
	close IN;

	unlink($tmpfile);

	# This is a multi-line regex. So, handle out of the above loop
	if ($file =~ m/are available in the [gG]it repository at:[\s\n]+([^\s]+\:[^\s]+)\s+([^\s]+)/) {
		$devel_tree = $1;
		$devel_branch = $2;
		printf "\tDeveloper's tree: $1 $2\n";
	}

	if (!$devel_tree && !$devel_branch) {
		return "FAILED: Can't parse the pull request!";
	}
	return "";
}

sub run_cmd($$)
{
	my $cmd = shift;
	my $drop_ignored_lines = shift;

	print "\$ $cmd\n";
	my ($stdout, $stderr, $exit) = tee {
		system($cmd);
	};

	return ($stdout, $stderr, $exit) if (!$drop_ignored_lines);

	# Check for matches for all lines of the ignore file
	my $err = "";
	open my $fh, "<", \$stderr;
	while (<$fh>) {
		my $discard = 0;
		foreach my $ln (@ignore) {
			if (m/($ln)/) {
				$discard = 1;
				print "discarding pattern '$ln':\n\t$_" if ($debug);
				last;
			}
		}
		$err .= $_ if (!$discard);
	}

	$stdout =~ s/\s+$//;
	$stdout = "\$ $cmd\n$stdout\n";

	return ($stdout, $err, $exit);
}

sub handle_quilt_tree()
{
	my $series = qx(quilt series);

	my $fatal = 0;
	my $nr_issues = 0;
	my $nr_patches = 0;
	my $pdf_err = 0;
	my $err;
	my $patch_err;

	# Restore git to original state and do a build
	qx(git reset --hard);

	my $start_time = time();

	foreach my $p (split(' ', $series)) {
		my $res = qx(quilt push $p);
		my $rc = $?;

		my $has_fatal = 0;
		my $has_issues = 0;
		undef $patch_err;

		if ($rc) {
		    $error = "FAILED: patch patch $p doesn't apply:\n$res";
		    return 0;
		}

		if (!$res) {
		    $error = "FAILED: quilt push didn't output anything";
		    return 0;
		};

		print "\tChecking $p:\n";

		# Just to avoid a warning
		system("touch build_yes/Module.symvers");
		system("touch build_mod/Module.symvers");

		my ($stdout, $stderr, $exit) = run_cmd("make O=build_yes allyesconfig", 1);
		$stderr =~ s/\n/\n\t/g;
		$stderr =~ s/\n\t$/\n/;
		$stdout =~ s/\n/\n\t/g;
		$stdout =~ s/\n\t$/\n/;
		if ($exit || $stdout =~ /WARNING/ || $stderr =~ /WARNING/) {
			$patch_err .= "\n    allyesconfig: error:\n\t" . $stdout . $stderr;
			$has_issues++;
		}
		foreach my $cmd (@make_yescmd) {
			my ($stdout, $stderr, $exit) = run_cmd($cmd, 1);

			if ($stderr) {
				$stderr =~ s/\n/\n\t/g;
				$stderr =~ s/\n\t$/\n/;
				print "\nError #$exit when running $cmd\n$stderr";
				$patch_err .= "\n    allyesconfig: return code #$exit:\n\t" . $stderr;
				$has_fatal++;
			}
		};

		if (!$test) {
			# Don't build with allmodconfig if test, in order to speedup things
			my ($stdout, $stderr, $exit) = run_cmd("make O=build_mod allmodconfig", 1);
			$stderr =~ s/\n/\n\t/g;
			$stderr =~ s/\n\t$/\n/;
			$stdout =~ s/\n/\n\t/g;
			$stdout =~ s/\n\t$/\n/;
			if ($exit || $stdout =~ /WARNING/ || $stderr =~ /WARNING/) {
				$patch_err .= "\n    allyesconfig: error:\n\t" . $stdout . $stderr;
				$has_issues++;
			}
			foreach my $cmd (@make_modcmd) {
				my ($stdout, $stderr, $exit) = run_cmd($cmd, 1);

				if ($stderr) {
					$stderr =~ s/\n/\n\t/g;
					$stderr =~ s/\n\t$/\n/;
					print "\nError #$exit when running $cmd\n$stderr";
					$patch_err .= "\n    allmodconfig: return code #$exit:\n\t" . $stderr;
					$has_fatal++;
				}
		}
		};

		qx(sed s,userspace-api/media,media/userspace-api, -i Documentation/Makefile);
		mkdir "Documentation/media";
		open OUT, ">Documentation/media/index.rst" or die "can't create a Documentation/media/index.rst file";
		print OUT "Linux Kernel Media Documentation\n";
		print OUT "================================\n\n";
		print OUT ".. toctree::\n";
		print OUT "\t:maxdepth: 4\n\n";
		print OUT "\tadmin-guide/index\n";
		print OUT "\tuserspace-api/index\n";
		print OUT "\tdriver-api/index\n";
		close OUT;
		qx(rsync -vuza --delete Documentation/admin-guide/media/ Documentation/media/admin-guide);
		qx(rsync -vuza --delete Documentation/driver-api/media/ Documentation/media/driver-api);
		qx(rsync -vuza --delete Documentation/userspace-api/media/ Documentation/media/userspace-api);

		foreach my $cmd (@doc_builds) {
			my ($stdout, $stderr, $exit) = run_cmd($cmd, 1);

			if ($exit) {
				$stderr =~ s/\n/\n\t/g;
				$stderr =~ s/\n\t$/\n/;
				print "\nError #$exit when running $cmd\n";
				$patch_err .= "\n    Error #$exit:\n\t" . $stderr;
				$has_issues++;
			}
		};

		my $cmd = "cat $p | $checkpatch";
		my ($stdout, $stderr, $exit) = run_cmd("$cmd", 1);
		if ($exit) {
			$stdout =~ s/\n/\n\t/g;
			$stdout =~ s/\n\t$/\n/;
			$stderr =~ s/\n/\n\t/g;
			$stderr =~ s/\n\t$/\n/;
			print "\nError #$exit when running $cmd\n";
			$patch_err .= "\n   checkpatch.pl:\n\t" . $stdout . $stderr;
			$has_issues++;
		}

		$err .= "\n$p:\n" . $patch_err if ($patch_err);

		# Increment per-patch counts
		$nr_patches++;
		$nr_issues++ if ($has_issues | $has_fatal);
		$fatal++ if ($has_fatal);
	}

	# Extra checks that won't need to be done for each single patch
	foreach my $cmd (@per_pr_builds) {
		my ($stdout, $stderr, $exit) = run_cmd($cmd, 1);

		if ($exit) {
			$stderr =~ s/\n/\n\t/g;
			$stderr =~ s/\n\t$/\n/;

			print "\nError #$exit when running $cmd\n" . $stderr;
			$err .= "\n\nError #$exit when building PDF docs\n";

			$pdf_err++;
			# PDF stderr is too noisy. Don't place it at $patch_err
		}
	};

	my $elapsed_time = time() - $start_time;

	return ($nr_patches, $nr_issues, $fatal, $pdf_err, $err, $elapsed_time);
}

sub clean_quilt_tree()
{
	qx(quilt pop -afq);

	my $series = qx(quilt series);
	foreach my $p (split(' ', $series)) {
		qx(quilt del $p);
	}

	qx(git reset -q --hard);
}

sub build_prepare()
{
	# Prepare for builds, doing an initial build without any patch applied
	run_cmd("make O=build_yes allyesconfig init modules_prepare", 0);
	run_cmd("make O=build_yes drivers/staging/media/", 0);
	run_cmd("make O=build_yes drivers/media/", 0);

	# Don't build with allmodconfig if test, in order to speedup things
	return if ($test);

	run_cmd("make O=build_mod allmodconfig init modules_prepare", 0);
	run_cmd("make O=build_mod drivers/staging/media/", 0);
	run_cmd("make O=build_mod drivers/media/", 0);
}

sub elapsed_time_string($)
{
	my $elapsed_time = shift;

	my $sec = $elapsed_time % 60;
	my $min = $elapsed_time / 60;
	my $min = $min % 60;
	my $hr = $min / 60;

	return sprintf ("%02d:%02d:%02d", $hr, $min, $sec);
}

sub send_email($$$$$$$$$)
{
	my $pr = shift;
	my $nr_patches = shift;
	my $nr_issues = shift;
	my $fatal = shift;
	my $pdf_err = shift;
	my $err = shift;
	my $elapsed_time = shift;
	my $gpg = shift;
	my $error = shift;

	$elapsed_time = elapsed_time_string($elapsed_time);

	# Generate a draft for an e-mail

	my $pr_name = "pull_request_#" . $pr;
	my $pr_number = " (#" . $pr . ")";

	$mbox =~ s,mbox/$,,;

	my $fname = "$work_dir/email_$pr_name";

	if (!open OUT, ">$fname") {
		printf "Can't create $fname\n";
		return;
	};

	my $msg_id;

	$msg_id = $1 if ($email_msg_id =~ m/\<(.*)\>/);

	print OUT "Date: " . strftime("%a, %d %b %Y %H:%M:%S %z", localtime(time())) . "\n";
	print OUT "From: builder\@linuxtv.org\n";
	# print OUT "To: $email_from, $maintainers\n";
	print OUT "Subject: Re: $email_subject$pr_number\n";
	print OUT "In-Reply-To: $email_msg_id\n";
	print OUT "\n";
	print OUT "Pull request: $mbox\n";
	print OUT "Build log: " . $ENV{'BUILD_URL'} . "\n" if ($ENV{'BUILD_URL'});
	print OUT "Build time: $elapsed_time\n" if ($elapsed_time);
	print OUT "Link: https://lore.kernel.org/linux-media/$msg_id\n" if ($msg_id);
	print OUT "\n$gpg\n" if ($gpg);

	my $pdf_msg;
	$pdf_msg = ", plus one error when buinding PDF document" if ($pdf_err);

	if ($error) {
		print OUT "\nBuild aborted due to a fatal error:\n$error\n";
		if ($nr_issues | $pdf_err) {
			print OUT "Also, got $nr_issues/$nr_patches patches with issues, being $fatal at build time$pdf_msg\n";
		}
	} else {
		if ($nr_issues | $pdf_err) {
			print OUT "Summary: got $nr_issues/$nr_patches patches with issues, being $fatal at build time$pdf_msg\n";
		} else {
			print OUT "Summary: no issues. All $nr_patches looked fine.\n";
		}
	}
	print OUT "\nError/warnings:\n$err\n" if ($err);

	close OUT;

	print "\$ git send-email --smtp-server localhost $fname --to='$maintainers'\n";

	if (!$debug) {
		my $email = qx(git send-email --smtp-server localhost $fname --to='$maintainers');
		print "EMAIL:\n\n$email\n";
	} else {
		my $email = qx(cat $fname);
		print "===> EMAIL to be sent:\n\n$email\n";
	}
}

#
# Find the head for media_stage master
#
sub media_stage_head()
{
	my $remote = undef;

	open IN, "git remote -v|";
	while (<IN>) {
		my @fields = split(/\s+/);

		if ($fields[1] eq "git://linuxtv.org/media_stage.git") {
			$remote = $fields[0];
			last;
		}
	}
	close IN;

	if (!$remote) {
		print("Cannot find remote for media_stage, please run `git remote add media_tree git://linuxtv.org/media_stage.git`\n");
		exit(1)
	}

	print("media_stage remote: ${remote}\n") if ($debug);

	my $head = undef;

	open IN, "git log -1 --oneline $remote/master |";
	while (<IN>) {
		my @fields = split(/\s+/);

		$head = $fields[0];
		last;
	}
	close IN;

	if (!$head) {
		print("Cannot find head for ${remote}/master\n");
	}

	print("media_stage master head commit: ${head}\n") if ($debug);

	return ($remote, $head);
}

#
# Checks for pending PR at patchwork
#
#
# Get emails sent to the patchwork instance
#
sub check_patchwork($)
{
	my $client = shift;
	my ($since, $old_last_pr, $last_pr);

	my $strp = DateTime::Format::Strptime->new(
		pattern => '%Y-%m-%d %T',
		time_zone => 'local',
	);

	if ($debug) {
		$last_pr = 0;
		$old_last_pr = 0;
		$since = "";
	} else {
		open IN, "$work_dir/since";
		while (<IN>) {
			if (m/Date:\s*(.*)/) {
				$since = $strp->parse_datetime($1);
				next;
			}
			if (m/Last-PR:\s*(.*)/) {
				$last_pr = $1;
				$old_last_pr = $1;
			next;
			}
		}
		close IN;
	}

	# Get up to 500 emails sent to the ML with subject that has "GIT"
	my $get="/api/1.0/patches/?order=-id&per_page=500&project=1&state=1&since=$since&q=GIT&archived=false";

	print "Getting URL $patchwork_instance$get\n" if ($debug);

	my @json;
	do {
		$client->GET($get);

		#print "Get from $get\n";

		# Parse all e-mails received by patchwork that match the above criteria
		my $new_json = from_json($client->responseContent(), {utf8 => 1});

		push(@json, @$new_json);

		undef $get;

		my $links = $client->responseHeader('Link');

		$get = $1 if ($links =~ m/\<([^\>]+)\>; rel="next"/);

		$get =~ s/^($patchwork_instance)//;
	} while ($get);

	print Dumper(@json) if ($debug > 1);

	my $n_pr = 0;
	foreach my $item(reverse(@json)) {
		undef $reference_cs;
		undef $final_patch;
		undef $pull_log;
		undef %lines_changed_pull_email;
		undef %lines_changed_git_pull;
		undef $devel_tree;
		undef $devel_branch;
		undef $email_from;
		undef $email_subject;
		undef $email_msg_id;

		$mbox = %$item{'mbox'};
		my $url = %$item{'pull_url'};
		my $item_date = %$item{'date'};

		my $pr;
		$pr = %$item{'id'};
		$pr = $1 if (!$pr && $mbox =~ m/(\d+)/);

		# On test mode, discards everything but $test PR
		next if ($test && ($pr ne $test));

		my $dt;
		$item_date =~ s/T/ /g;
		$dt = $strp->parse_datetime($item_date);

		# Ignore already handled pull requests
		next if ($pr <= $old_last_pr);

		printf "\n" . "=" x 79 . "\n\n" if ($n_pr);
		printf "Handling PR for $url.\n\tMail box: $mbox\n";
		printf "\tDate: %s\n", $strp->format_datetime($dt) if ($dt);

		# Update since logic to avoid handling patches again
		# We do it before processing the e-mail, as, this way, if
		# something gets wrong, it won't parse the same thing again
		if (!$since && $dt) {
			$since = $dt;
		} elsif ($dt && $dt > $since) {
			$since = $dt;
		}
		if (!$last_pr && $pr) {
			$last_pr = $pr;
		} elsif ($pr) {
			$last_pr = $pr if ($pr > $last_pr);
		}

		if (!$debug) {
			open OUT, ">$work_dir/since";
			printf OUT "Date: %s\n", $strp->format_datetime($since) if ($since);
			print OUT "Last-PR: $pr\n" if ($pr);
			close OUT;
		}

		if ($url) {
			my $gpg;
			$n_pr++;

			clean_quilt_tree();
			build_prepare();

			$error = handle_pull_request_email($mbox);

			$gpg = pull_request_to_quilt() if (!$error);

			my ($nr_patches, $nr_issues, $fatal, $pdf_err, $err, $elapsed) = handle_quilt_tree() if (!$error);

			print "Build aborted due to error(s):\n$error" if ($error);

			send_email($pr, $nr_patches, $nr_issues, $fatal, $pdf_err, $err, $elapsed, $gpg, $error);
		}
		# TODO: add support for non-pull requests
	}

	if (!$n_pr) {
		print "Nothing to pull\n";
		unlink "$work_dir/pr_count";
	} else {
		open OUT, ">$work_dir/pr_count";
		print OUT "$n_pr\n";
		close OUT;
		printf "$n_pr pull requests handled.";
		printf " Will start next time from %s.",
		       $strp->format_datetime($since) if ($since);
		printf " Last patch: #$last_pr", if ($last_pr);
		print "\n";
	}
	printf "Total elapsed time: %s\n", elapsed_time_string(time() - $start_script_time);

}

#
# MAIN
#

foreach my $cmd (@ARGV) {
	if ($cmd eq "--debug") {
		$debug++;
	} elsif ($branch_to_test) {
		print "Usage: $0 [<--debug>] branch_to_test\n";
		exit(1);
	} else {
		$branch_to_test = $cmd;
	}
}

if (!$branch_to_test) {
	print "Usage: $0 [<--debug>] branch_to_test\n";
	exit(1);
}

#
# Gather the contents of the supress_warnings file
#
open IN, "$suppress_warnings";
while (<IN>) {
	# Ignore empty lines and comments
	next if (m/^\s*$/);
	next if (m/^\s*\#/);
	s/\n//;
	push @ignore, $_;
	printf "Ignore line: $_\n" if ($debug);
};
close IN;

# Clean any existing quilt patch
clean_quilt_tree();

#
# Create a clean worktree
#
if (!$no_cleanup) {
	print "Cleaning working dir\n";

	qx(git branch -D $tmpbranch);
	# Ensure a clean working directory
	qx(make mrproper);
	qx(git clean -fqd -e build_mod -e build_yes);

}

# Create a quilt series
branch_to_quilt();

# Just for sanity and tests: drop any previously created temp branch
qx(git branch -D $tmpbranch) if (qx(git branch|grep $tmpbranch));

handle_quilt_tree();
print "Build aborted due to error(s):\n$error" if ($error);

__END__

=head1 NAME

jenkins_handle_pr.pl - Handle pull requests via Jenkins

=head1 SYNOPSIS

B<jenkins_handle_pr.pll> [--debug] [--no-cleanup] [--test <PR ID>] [--help] [--man]

=head1 OPTIONS

=over 8

=item B<--debug> path

Activate it in debug mode

=item B<--no-cleanup> path

Don't run the initial step of cleaning up the git tree.

=item B<--test [PR#]> path

Activate it in debug mode, getting just the specific pull requrest ID

=item B<--help>

Prints a brief help message and exits.

=item B<--man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

Handle pull requests via Jenkins.

=head1 BUGS

Report bugs to Mauro Carvalho Chehab <mchehab@kernel.org>

=head1 COPYRIGHT

Copyright (c) by Mauro Carvalho Chehab <mchehab@kernel.org>.

=cut
